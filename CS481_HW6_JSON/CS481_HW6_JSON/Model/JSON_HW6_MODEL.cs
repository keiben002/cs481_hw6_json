﻿namespace CS481_HW6_JSON.Model
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;



    /*
     * This is the Definition class is a C# model to consume the JSON from the owlbot.info API that is accepted inside of the MainPage.xaml.cs file 
     */
    public partial class Definition
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string DefinitionDefinition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }

        [JsonProperty("image_url")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("emoji")]
        public object Emoji { get; set; }
    }

    /*
     * This definition class converts a string into an object 
     */
    public partial class Definition
    {
        public static Definition[] FromJson(string json) => JsonConvert.DeserializeObject<Definition[]>(json, CS481_HW6_JSON.Model.Converter.Settings);
    }

    /*
     * Serialization converts the specific object into a string 
     */
    public static class Serialize
    {
        public static string ToJson(this Definition[] self) => JsonConvert.SerializeObject(self, CS481_HW6_JSON.Model.Converter.Settings);
    }

    /*
     * This class handles conversion of a string -> JSON and from JSON -> string in the respective Definition and Serialize classes 
     */
    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
