﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Net.Http;
using CS481_HW6_JSON.Model; //using model class with JSON -> C# data that is used to produce parse information 
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CS481_HW6_JSON
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            if (!Connectivity.ConnectionProfiles.Contains(ConnectionProfile.WiFi))   //If there is no wifi/internet connection then it will display an error message upon a new main page 
            {
                DisplayAlert("Error: You are not connected to the internet", "", "OK");     
            }

            headlines.Text = "↓ Terminology & Information ↓";                  //displays an informational title
        }



        /*
         * This function Parse_Clicked is activated when the user clicks the button inside the app after entering a word inside the entry box. Depending on the user input and when the input is correct  
         * the button will parse JSON data from https://owlbot.info that will display the type/grammar, the definition and an example of the word from the site. If the user's input was incorrect for  
         * a variety of reasons then it will display a corresponding error message. 
         */

        async void Parse_Clicked(object sender, EventArgs e)
        {
            Internet_con.Text = "";
            null_value.Text = "";
            bad_req.Text = "";
            Type_pre.Text = "";
            T.Text = "";                //after the button is pressed, clear all previous data  
            Def_pre.Text = "";
            D.Text = "";
            Example_pre.Text = "";
            E.Text = "";

            if (Connectivity.ConnectionProfiles.Contains(ConnectionProfile.WiFi))   //Correct Input; Connectivity; Case 1: First check; If the user has internet/wifi then this if statement will display true 
            {                                                                       //otherwise false and goes to internet failed error case                                                                    

                var input = user_input.Text; //stores the string input entered into the variable input

                if (!String.IsNullOrEmpty(input) && input.Trim().Length != 0)  //Correct Input; Wording; Case 2: Second check; if the input is not null or empty AND the input's length after removing whitespaces 
                {                                                              //is not 0, then it will be true and enter the if statement. Otherwise false and goes to the error case for Wording.
                    null_value.Text = "";

                    HttpClient client = new HttpClient();        //created new HTTP object

                    var uri = new Uri(string.Format("https://owlbot.info/api/v2/dictionary/" + input));  //stores api inside of a uri


                    //retrieves the uri through the HTTP GET request method 
                    var request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = uri
                    };

                    //the client sends the request(HttpRequestMessage) that we generated from the GET method into a HttpResponseMessage
                    HttpResponseMessage response = await client.SendAsync(request);

                    
                    Definition[] def = null;
                    //initializes C# temp object for access to the class in the JSON_HW6_MODEL.cs model file

                    /*
                     * Array's in owlbot.info JSON
                     * 
                     * Array's are used in this JSON because for each word there are multiple definitions, types and examples that this site sends per response  
                     * so for example the word "hello" might have two definitions but one type and five or six examples. To keep it simple, I get the first definition, type and example of the 
                     * word and then display those strings parsed from JSON.
                     */

                    if (response.IsSuccessStatusCode) //Correct case; HTTP Status Code; Case 1: If response message was in successful range (200 - 299), otherwise false
                    {

                        var content = await response.Content.ReadAsStringAsync();       //read JSON content to the JSON_HW6_MODEL.cs
                        def = Definition.FromJson(content);                     //Deserialize JSON content from JSON to C# into our C# object 

                        Type_pre.Text = "Type/Grammar";
                        Type_pre.TextDecorations = TextDecorations.Underline;    
                        T.Text = def[0].Type;                                   //Accesses the first type using the C# object and displays it with the T label
                        Def_pre.Text = "Definition";                            
                        Def_pre.TextDecorations = TextDecorations.Underline;
                        D.Text = def[0].DefinitionDefinition;                   //Accesses the first definition using the C# object and displays it with the D label
                        Example_pre.Text = "Example";
                        Example_pre.TextDecorations = TextDecorations.Underline;
                        E.Text = def[0].Example;                                //Accesses the first example using the C# object and displays it with the E label

                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.NotFound) //Error Case 1; HTTP Status Code; Error 404: If the user fails to enter an acceptable word after pressing the button. Clear all 
                    {                                                                   //formatting and display an error message for "no definition found"
                        Type_pre.Text = "";
                        T.Text = "";
                        Def_pre.Text = "";
                        D.Text = "";
                        Example_pre.Text = "";
                        E.Text = "";
                        null_value.Text = "Error: No definition found!";
                    }
                    else if (response.StatusCode == System.Net.HttpStatusCode.BadRequest) //Error Case 2; HTTP Status Code; Error 400: If the user has created a corrupted/unreadable request. If true then  
                    {                                                                     //clear all previous data and display "Error: Bad request"
                        Type_pre.Text = "";
                        T.Text = "";
                        Def_pre.Text = "";
                        D.Text = "";
                        Example_pre.Text = "";
                        E.Text = "";
                        bad_req.Text = "Error: Bad request!";
                    }
                }
                else  //Error Case; Wording; Case 2: If the user entered an input that is either null or empty or has only blank spaces then this else statment will be true. If entered it will clear all previous format and   
                {     //display "Error: Incorrect input."
                    Type_pre.Text = "";
                    T.Text = "";
                    Def_pre.Text = "";
                    D.Text = "";
                    Example_pre.Text = "";
                    E.Text = "";
                    null_value.Text = "Error: Incorrect input.";
                }
            }
            else //Error case; Connectivity; Case 2: if the user has no internet and tried to enter a word, it will clear all previous formatting and an error message will be displayed. This will continue to repeat everytime the button is
            {    //clicked until the user restores the internet.
                
                Type_pre.Text = "";
                T.Text = "";
                Def_pre.Text = "";
                D.Text = "";
                Example_pre.Text = "";
                E.Text = "";
                Internet_con.Text = "Error: Failed to connect to the internet.";

            }
        
        }
    }
}
